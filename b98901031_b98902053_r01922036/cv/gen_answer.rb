#!/bin/ruby
fout = File.open "basic.ans.#{ARGV[0]}", "w"
File.open("basic.test.#{ARGV[0]}", "r") do |f|
  while line = f.gets
    fout.puts (line.split(' ').reject{|w|w.empty?})[1]
  end
end