#include <cstdio>
#include "webpage.h"

extern Webpage webpages[];

void Webpage::read(char const trainfile[]){
  FILE* fin = fopen(trainfile, "r");
  int i; char t[20], u[20];
  while( EOF != fscanf(fin, "%d %s %s", &i, t, u) ){
    webpages[i].id = i;
    webpages[i].university = string(u);
    webpages[i].type = string(t);
  }
  fclose(fin);
}

void Webpage::read_links(char const linkfile[]){
  FILE* fin = fopen(linkfile, "r");
  int i, num, cited;
  while( EOF != fscanf(fin, "%d %d", &i, &num) ){
    webpages[i].links.resize(num);
    for(int j = 0; j<num; ++j){
      fscanf(fin, "%d", &cited);
      webpages[i].links[j] = cited;
      webpages[cited].referrers.push_back(i);
    }
  }
  fclose(fin);
}

void Webpage::read_words(char const wordfile[]){
  FILE* fin = fopen(wordfile, "r");
  int i, num, word;
  while( EOF != fscanf(fin, "%d %d", &i, &num) ){
    webpages[i].words.resize(num);
    for(int j = 0; j<num; ++j){
      fscanf(fin, "%d", &word);
      webpages[i].words[j] = word;
    }
  }
  fclose(fin);
}

void Webpage::read_titles(char const titlefile[]){
  FILE* fin = fopen(titlefile, "r");
  int i, num, title;
  while( EOF != fscanf(fin, "%d %d", &i, &num) ){
    webpages[i].titles.resize(num);
    for(int j = 0; j<num; ++j){
      fscanf(fin, "%d", &title);
      webpages[i].titles[j] = title;
    }
  }
  fclose(fin);
}