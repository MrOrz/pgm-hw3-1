#ifndef __WEBPAGE__
#define __WEBPAGE__
#include <vector>
#include <string>
using namespace std;

class Webpage{
public:

  Webpage():id(0){}

  // Populate the global webpages array defined in train.cpp.
  static void read(const char*);
  static void read_links(const char*);
  static void read_titles(const char*);
  static void read_words(const char*);

  int id;
  string university;
  string type;
  vector<int> titles;
  vector<int> links;     // The webpages that this page links to.
  vector<int> referrers; // The webpages that links to this page.
  vector<int> words;

  bool is_read() const{
    return id != 0;
  }

};


#endif