#include "webpage.h"

using namespace std;

// Manually increase the number of webpages here or it will cause segmentation
// fault.
#define NUM_WEBPAGES 1500

// The global array that is shared with Webpage methods.
Webpage webpages[NUM_WEBPAGES];

void train(){
  for(int i=0; i<NUM_WEBPAGES; ++i){
    Webpage& p = webpages[i];
    if(! p.is_read()) continue; // Skip unread pages

    // TODO:
    // The webpages are now populated with the test data.
    // Do the training here and output necessary model file for the tester.

    // printf("[%s] %d -- %s, %ld links, %ld words, %ld words in title.\n",
    // p.type.c_str(), p.id, p.university.c_str(), p.links.size(), p.words.size(), p.titles.size());
  }

}

void test(char const testfile[]){
  FILE *fin = fopen(testfile, "r"),
       *fout = fopen("test.pred.txt", "w");
  int id; char university[20], unused[20];

  while( EOF != fscanf(fin, "%d %s %s", &id, unused, university)){
    string u = string(university);
    string type = "";
    // TODO:
    // Given the id and the university, output the type of the page.

    // stub: The perfect predictor that gives 100% accuracy in cross validation
    //       by peeking the answer.
    // type = string(unused);

    // stub2: Always guess "student".
    type = string("student");

    fprintf(fout, "%s\n", type.c_str());
  }

  fclose(fin); fclose(fout);
}


void print_usage(){
  printf("Usage:\n./main <train> <link> <word> <title> <test>\n\n");
}

int main(int argc, char const *argv[])
{
  if(argc != 6){
    print_usage();
    return 1;
  }

  // Setup the global webpages array.
  Webpage::read(argv[1]);
  Webpage::read_links(argv[2]);
  Webpage::read_words(argv[3]);
  Webpage::read_titles(argv[4]);

  // Start training.
  train();
  test(argv[5]);

  return 0;
}