import sys

## Predictions
f = open(sys.argv[1], "r")
pred = map(lambda x: x.strip(), f.readlines())

## Ground truth
f = open(sys.argv[2], "r")
truth = map(lambda x: x.strip(), f.readlines())

## Confusion matrix
confusion = {}

for i in range(0, len(truth)):
  if not confusion.has_key(truth[i]):
    confusion[truth[i]] = {}
  if not confusion[truth[i]].has_key(pred[i]):
    confusion[truth[i]][pred[i]] = 0

  confusion[truth[i]][pred[i]] += 1


## Printing
columns = confusion.keys()
print "\t".join(["  t\p"] + columns)
correct = 0.0
wrong = 0.0

for c1 in columns:
  row = [c1]
  for c2 in columns:
    count = 0
    try:
      count = confusion[c1][c2]
    except: pass
    row.append(str(count))

    if c1 == c2:
      correct += count
    else:
      wrong += count

  print "\t".join(row)

print "Accuracy: ", correct / (correct + wrong)